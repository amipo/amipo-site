# site

preview: [![pipeline status](https://framagit.org/amipo/amipo-site/badges/preview/pipeline.svg)](https://framagit.org/amipo/amipo-site/commits/preview)

master: [![pipeline status](https://framagit.org/amipo/amipo-site/badges/master/pipeline.svg)](https://framagit.org/amipo/amipo-site/commits/master)


# Site web de l'AMIPO

Ce site est construit avec Hugo.

## Branche preview
Les sources commitées sur la branche preview sont construite par le pipeline et configuré pour être visualisable dans les "gitlab pages". La prévisulisation est ici : https://amipo.frama.io/amipo-site/

## Branche master
Les sources commitées sur la branche master sont construite par le pipeline pour la prod. Le site static construit est disponible en téléchargement ici : https://framagit.org/amipo/amipo-site/-/jobs/artifacts/master/download?job=prod


