---
title: "Conditions Générales d'Utilisation"
date: 2020-02-11T22:55:03+01:00
author: b01
tags: [cgu, legal]
draft: false
---

# CGU des services proposés par l'AMIPO

## Préambule 

En utilisant les services de l'AMIPO, vous acceptez d’être lié par les conditions générales d'utilisation (CGU) suivantes.
L'AMIPO se réserve le droit de mettre à jour et modifier ces conditions de temps à autre.
 
Pour en faciliter la lecture, sans le blabla juridique, nous vous en proposons ci-dessous une version compréhensible par un être humain normal.
 

## Des services libres et gratuits, adhérez !

Sauf mention contraire, vous pouvez utiliser librement les services proposés par l'association, nous vous invitons néanmoins à adhérer à l'association et à vous impliquer dans ses activités, à venir rencontrer les membres à une date indiquée dans le [calendrier](https://amipo.fr/agenda/).
 
## Nous respectons vos données et plus

L'AMIPO n’exploitera vos données personnelles que dans trois situations:
- la production éventuelle de statistiques internes, anonymisées et agrégées
- vous prévenir d’un changement important sur le service
- vous inviter à l'Assemblée Générale

 
L'AMIPO ne transmettra ni ne revendra vos données personnelles (votre vie privée nous tient - vraiment - à cœur).
Votre contenu vous appartient, mais nous vous encourageons à le publier sous licence libre.
Les engagements de l'AMIPO vis à vis des services résultent d’une démarche militante en accord avec la charte du Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires.
 
## Vous pouvez proposer votre aide

Que ce soit pour l'administration du serveur, la promotion des services fournis, l'organisation, l'accueil des membres ...
 
# Mais ...
 
## Vous êtes entièrement responsables de ce que vous faites

L'AMIPO n'est pas là pour vous couvrir et prendre des risques légaux à votre place, même si votre action est légitime. 
En conséquence, l'AMIPO  fera du mieux qu'elle peut avec ses obligations légales.
 
## Si ça casse, nous ne sommes pas obligés de réparer

L'AMIPO propose des services de façon bénévole. Si le service est indisponible ou si vos données sont perdus, ce sera une faute collective. Pour les services avec un abonnement, le contrat indique comment ça se passe dans ce cas.
 
## Si vous n’êtes pas contents, n'exigez rien, agissez

Si le service ne vous convient pas, libre à vous:
- de participer à sa gestion pour l'améliorer (en accord avec les autres membres de l'association)
- d’en trouver un équivalent (ou meilleur) ailleurs
- de monter le vôtre ;

## Tout abus peut entraîner la fermeture d'un compte

Si un utilisateur abuse du service, par exemple en monopolisant des ressources machines partagées, son contenu ou son compte pourra être supprimé, si nécessaire sans avertissement ni négociation. L'AMIPO reste seul juge de cette notion « d’abus » dans le but de fournir le meilleur service possible à l’ensemble de ses membres. Si cela vous parait anti-démocratique, anti-libriste, anti-liberté-d’expression, merci de vous référer à la clause précédente ;
 
## Rien n’est éternel

Nos services peuvent fermer (par exemple: faute de fonds pour les maintenir), ils peuvent être victimes d’intrusion (le « 100 % sécurisé » n’existe pas). Nous vous encourageons donc à conserver une copie de vos données, car l'AMIPO ne saurait être tenu pour responsable de leur hébergement sans limite de temps.
