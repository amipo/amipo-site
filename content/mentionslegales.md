---
title: "Mentions légales"
date: 2020-02-11T22:55:03+01:00
author: b01
tags: [mentions-legales, legal]
draft: false
---

## Éditeur
AMIPO
Association loi 1901 déclarée en sous-préfecture d’Orleans le 23 mars 2017  sous le n° 899, publiée au JO du 1 avril 2017

Direction de la publication : Conseil d'Administration Collégial de l'association

N° Siret : 829 782 275 00012

## Siège social

Association AMIPO

108 Rue due Bourgogne 
45000 ORLEANS
FRANCE

Pour nous contacter : https://amipo.fr  `infos [chez]* amipo.fr`

## Hébergeur - fournisseur de machines virtuelles

Alsace Réseau Network - http://arn-fai.net/

## Informatique et libertés
### Informations personnelles collectées
En France, les données personnelles sont notamment protégées par la loi n°78-17 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L.226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.

En tout état de cause Amipo ne collecte des informations personnelles relatives à l’utilisateur (nom, adresse électronique, coordonnées téléphoniques) que pour le besoin des services proposés par les sites du réseau AMIPO, notamment pour l’inscription à des espaces de discussion par le biais de formulaires en ligne ou pour des traitements statistiques. L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur des sites du réseau AMIPO le caractère obligatoire ou non des informations qu’il serait amené à fournir.

### Analyse statistique et confidentialité
L'AMIPO souhaite être particulièrement respectueuse de vos données et de votre vie privée, en conséquence, nous attachons à fournir des services qui implémentent autant que possible ces principes. L'éventuelle analyse statistique de l'usage de ce site ou des services sera réalisée uniquement pour suivre et améliorer la qualité de ces services. En aucun cas, l'AMIPO ne sera amenée à partager ou à vendre ces données à des tiers.


### Rectification des informations nominatives collectées
Conformément aux dispositions de l’article 34 de la loi n°48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant. Pour ce faire, l’utilisateur envoie à AMIPO :
- un courrier électronique à  `infos [chez]* amipo.fr`
- un courrier à l’adresse du siège de l’association (indiquée ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées physiques et/ou électroniques, ainsi que le cas échéant la référence dont il disposerait en tant qu’utilisateur du site AMIPO.
La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.

## Limitation de responsabilité
Ce site comporte des informations mises à disposition par des communautés, des structures ou des personnes via des liens hypertextes vers d’autres sites qui n’ont pas été développés par AMIPO. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces informations avec discernement et esprit critique. La responsabilité d' AMIPO ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.

AMIPO ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.

AMIPO ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.

Des espaces interactifs [tels qu'indiqués ici](https://framagit.org/amipo/amipo-plante) sont à la disposition des utilisateurs sur le site AMIPO. AMIPO se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, AMIPO se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie…).

## Propriété intellectuelle

Les contenus sont publiés sous la responsabilité des utilisateurs.

**Mentions légales de l’association AMIPO - le 11 février 2020**

