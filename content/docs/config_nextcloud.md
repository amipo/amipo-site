---
title: "Documentation de la config Nextcloud"
date: 2020-01-28T10:40:30+01:00
author: jChatard
tags: [doc, conf, admin]
tableOfContents: true
draft: false
---

Documentation de la configration de l’instance Nextcloud de l’Amipo : https://amipo.fr/nuage/

## Configuration minimale des services Nextcloud

Pour simplifier l'usage de notre nuage, et pour nous concentrer sur les services d'Agenda, de Contacts et de Tâches, et leur partage, nous configurons Nextcloud pour proposer le minimum de services.

Pour ce faire, dans le menu d'administration des applications https://amipo.fr/restore_nuage/settings/apps/installed#, nous ne laissons activées que les applications suivantes :
*  Brute-force settings 
*  Contacts 
*  Markdown Editor
*  Calendar
*  File sharing
*  Log Reader (Optionnelle, pour le moment cassé par la config syslog)
*  Monitoring
*  NextBackup (Optionnelle, backup la BD en plus de nos backups)
*  Password policy
*  Privacy
*  Quota warning
*  Registration
*  Right click
*  Tasks
*  Theming
*  Update notification (Est-ce vraiment nécéssaire ?)

## Mise à jour de Nextcloud

Se connecter au serveur via SSH.

<WRAP center round important 60%>
Faire un backup TODO FIXME
</WRAP>


Passer en user www-data, puis lancer l’upgrade :

    $ sudo su -l www-data -s /bin/bash
    $ cd /var/www/nuage/
    $ ./occ upgrade

Croiser les doigts LOL

## Inscription des membres

Par défaut, Nextcloud ne permet pas aux internautes de s'enregistrer par eux-mêmes.

Il faut utiliser le module complémentaire [Registration](https://apps.nextcloud.com/apps/registration) disponible depuis [l'interface de Nextcloud](https://amipo.fr/nuage/settings/apps).

Le module doit être configuré, afin de spécifier le rôle qui sera attribué aux internautes s'inscrivant sur le site. Le paramétrage est réalisé sur la page : [Paramètres supplémentaires](https://amipo.fr/nuage/settings/admin/additional).

Dans la section **Inscription**, la liste déroulante nommée :

- Groupe par défaut auquel appartiennent tous les utilisateurs

Sélectionner le groupe **Tout public**.

**Tout public** étant le groupe paramétré dans Nextcloud pour accueillir les internautes (sans rôle d’administration).


## Configuration des logs

Par défaut Nextcloud intègre un système de rotation des logs. Nous avons choisis de ne pas l'utiliser et d'utiliser à la place le couple ultra rodé rsyslog + logrotate.

### Config de Nextcloud
Fichier /var/www/nuage/config/config.php
``` .php
  'log_type' => 'syslog',
  'syslog_tag' => 'nuage-prod',
  'log_file' => '',
  'loglevel' => 2,
  'log_rotate_size' => 0,
```

### Config de rsyslog
Fichier /etc/rsyslog.d/nextcloud_nuage-prod.conf
``` .conf
 Messages from "nuage-prod"
:programname, isequal, "nuage-prod" /var/log/nextcloud/nuage-prod.log
```

### Config de logrotate
Fichier /etc/logrotate.d/nextcloud
``` .conf
/var/log/nextcloud/*.log 
{
        rotate 10
        daily
        maxsize 100M
        create 0640 www-data www-data
        missingok
        notifempty
        compress
        sharedscripts
        postrotate
                /usr/lib/rsyslog/rsyslog-rotate
        endscript

}
```


Les fichiers de logs sont placés dans /var/log/nextcloud par rsyslog :


