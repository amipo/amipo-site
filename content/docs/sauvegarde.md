
---
title: "Documentation des Sauvegardes AMIPO"
author: mxBossard
date: 2020-01-28T14:10:59+01:00
tags: [doc, conf, admin]
tableOfContents: true
draft: false
---

## Réalisation des Sauvegardes

### Fichiers sauvegardés

*  Le schéma de la base de données 
*  Les données de la base de données
*  Les données hors base (le système de fichier /var/nextcloud/data)
*  Les fichiers exécutables de l'application nextcloud de prod (/var/www/prod/nuage)
*  Les fichiers de logs du nextcloud de prod (/var/log/nuage-prod.log\*)
*  Le dépôt git de backup de etckeeper (/backups/etckeeper-git)

### Scripts de Sauvegarde

Les scripts de backups sont entreposés ici : https://framagit.org/amipo/amipo-admin-code/tree/master/backups
Actuellement il y a 2 scripts :
*  backup_nextcloud.sh : effectue le backup de l'instance nextcloud de prod
*  restore_nextcloud_maintenance.sh : s'assure que l'instance nextcloud de prod n'est plus en mode maintenance


### Principes de la Sauvegarde

-  Création d'un nouveau dossier pour recueillir les fichiers à sauvegardées dans le dossier conteneur de backups (/backups/nextcloud sur le FS / pour profiter de ses 20 GB que l'on ne peut pas réduire)
-  Mise en mode maintenance de l'instance Nextcloud (mode maintenance ON)
-  Création du dump de la BD (schéma et data)
-  Copie du système de fichier "data" de Nextcloud
-  Remise en route de l'instance Nextcloud (mode maintenance OFF)
-  Copie des autres systèmes de fichiers (exécutables, logs, etckeeper, ...)
-  Création d'une archive compréssée au format tar.gz à partir du nouveau dossier créé à l'étape 1.
-  Suppression du dossier créé à l'étape 1.
-  Vérification et Suppression des anciennes archives vieilles de plus de 7 jours.


### Sécuritées

-  Actuellement, la suppression des archives n'est effectué que pour les archives vieilles de plus de 7 jours, et si il existe au moins 7 fichiers présent dans le dossier contenant les archives.
-  Le script de backup ne plante pas en cas d'erreur de copie de fichiers pour tenter de backup le plus possibles de données. Pour chaque plantage durant une copie, un compteur est incrémenté pour indiquer le nombre d'erreur de copie dans le nom de l'archive et ainsi indiquer la "qualité" du backup.
-  Si le script de backup vient à planter, on execute alors le script restore_nextcloud_maintenance.sh pour ne pas laisser l'instance nextcloud en mode maintenance.
-  Un rapport est envoyé par email après chaque déroulement des backups. Ce rapport contient les logs des scripts, et un état du dossier contenant les ancienes archives (nombre d'archives et espace disque restant).


### Automatisation de la Sauvegarde

*  Le backup est automatisé par un cronjob de l'utilisateur www-data. Pour le voir : `crontab -l -u www-data`. Cela devrait être : 
``` cron
# Nextcloud backup
0  2  *  *  * ( export PGPASSWORD="XXXXX"; /var/amipo-admin-code/backups/backup_nextcloud.sh 2>&1 || /var/amipo-admin-code/backups/restore_nextcloud_maintenance.sh 2>&1 ) | mailx -s "[AMIPO Admin] Sorties de backup nocturne de Gits." admin
```
*  Le cronjob est exécuté chaque nuit à 02h00.
*  Les sorties d'erreur et standard du backup sont envoyés par email à l'alias admin qui est configuré dans /etc/aliases


### Sauvegarde de la base de données
*  Les sauvegardes de la base de données sont séparées en 2 fichiers : la structure et les données.
*  La sauvegarde de la structure des données est sauvegardé au format texte compressé (.psql.gz).
*  Les données sont sauvegardé au format custom postgres déjà compréssé (.custom), cela permet une restauration plus rapide.
*  On ne sauvegarde que le contenu du schéma "nextcloud".
*  Un utilisateur spécifique dans la BD réalise les sauvegardes : "nextcloud_backup" avec des privilèges minimaux.


## Restauration d'une Sauvegarde

### Création de la BD dans lequel sera effectué la restauration

```bash
$ sudo su - postgres
$ psql
```

```psql
create role restore_user;
alter role restore_user encrypted password 'foo';
alter role restore_user login;

alter role restore_user set search_path to nextcloud;
create database restore_nextcloud;
\c restore_nextcloud;
create schema nextcloud;
grant usage on schema nextcloud to restore_user;
\q
```

Quitter le compte postgres :

```bash
$ exit
```

### Restauration de la BD

``` bash
$ sudo mkdir /var/restore
$ sudo chown max:max /var/restore
$ cd /var/restore
$ sudo tar -xzf /backups/nextcloud/latest
$ psql -U restore_user -h localhost -d restore_nextcloud -c "show search_path;"
$ zcat [backup_name]/schema-nextcloud-dump.psql.gz | sudo -u postgres -s psql -d restore_nextcloud
$ sudo -u postgres -s pg_restore -d restore_nextcloud [backup_name]/data-nextcloud-dump.custom
$ sudo su - postgres
$ psql
```

```psql
\c restore_nextcloud;
grant all on all tables in schema nextcloud to restore_user;
grant all on all sequences in schema nextcloud to restore_user;
\q
```

```bash
$ exit
```


### Duplication de l'app nextcloud

``` bash
$ sudo mkdir -p /var/www/test/restore_nuage
$ sudo chown www-data:www-data /var/www/test/restore_nuage
$ sudo cp -ar [backup_name]/app/* /var/www/test/restore_nuage/
```
Edition du fichier /var/www/test/restore_nuage/config/config.php pour se connecter la BD:

```bash
$ sudo vim /var/www/test/restore_nuage/config/config.php
```


``` php
'trusted_domains' =>
  array (
    0 => 'test.amipo.fr',
  ),
'datadirectory' => '/var/restore/[backup_name]/data',
'overwrite.cli.url' => 'https://test.amipo.fr/restore_nuage',
'dbname' => 'restore_nextcloud',
'dbuser' => 'restore_nextcloud',
'dbpassword' => '***',
// ...
'syslog_tag' => 'restore_nuage',
```

Ajouter un cronjob
``` bash
$ sudo mkdir -p /var/log/restore_nextcloud/
$ sudo chown root:www-data /var/log/restore_nextcloud/
$ sudo chmod 0660 /var/log/restore_nextcloud/
$ sudo crontab -e -u www-data
*/5  *  *  *  * php -f /var/www/restore_nuage/cron.php 2>&1 /var/log/restore_nextcloud/cron.log
```

### Config Nginx

``` bash
sudo cp /etc/nginx/nextcloud.conf /etc/nginx/restore_nextcloud.conf
```

Remplacement de /nuage par /restore_nuage dans ce nouveau fichier.
Ajout dans /etc/nginx/sites-available/amipo.fr de include /etc/nginx/restore-nextcloud.conf;

