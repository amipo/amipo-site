---
title: "Gestion de conf du serveur"
date: 2020-02-19T18:01:54+01:00
author: 
tags: [doc, conf, install, admin]
tableOfContents: true
draft: false
---

## Gestion de la configuration du serveur
Nous utilisons le logiciel _etckeeper_ pour gérer la configuration du serveur. Ce logiciel archive tous les fichiers présent dans le répertoire _/etc/_ du serveur dans un dépôt git local.

Documentation officielle d'etckeeper : http://etckeeper.branchable.com/README/

### Consulter les modifications précédentes
Pour consulter les modications précédement commités, voici quelques lignes de commandes :
- Lister les commits basiquement: `sudo etckeeper vcs log`
- Lister les commits sous forme de graph: `sudo etckeeper vcs log --pretty=oneline --abbrev-commit --graph --decorate`
- Lister les stats des fichiers modifiés dans chaque commit: `sudo etckeeper vcs log --stat`
- Lister les modifications d'un commit (le contenu d'un patch): `sudo etckeeper vcs show COMMIT_HASH`
- Lister toutes les modifications depuis un commit: `sudo etckeeper diff COMMIT_HASH`
- Lister les modifications non commités en cours: `sudo etckeeper vcs status`

### Commiter vos modifications
Lorsque vos modifications de configuration sont réalisés sur le serveur, vous devez les commiter (to commit) en y joignant un message expliquant la modification. Pour cela, __veillez bien à utiliser la commande: `sudo etckeeper commit -m "MON_MESSAGE_DE_COMMIT"`__
Réaliser cette commande avec sudo permet de tracer l'auteur de la modification. 

### Sauvegardes de la configuration
Lorsque vous réalisez un commit, vos modifications sont automatiquement poussés vers 2 dépôt git :
- un dépot local : /backups/etckeeper-git
- un dépot distant : git@framagit.org:amipo/amipo-etckeeper.git

#### En local
Le dépôt local est un simple dépôt git "bare". Il est sauvegardé par le script de sauvegarde nocturne, [cf la documentation des sauvegardes](/docs/sauvegarde)

#### Sur la forge git
Le dépôt distant est hébergé sur la forge git de l'AMIPO. [Il est visitable ici](https://framagit.org/amipo/amipo-etckeeper). Ce dépôt est privé, car la configuration du serveur est sensible. Une clé de déploiement à été ajouté sur ce dépôt pour permettre à etckeeper d'y pousser les modifications.

### Installation
Voici les lignes de commande pour procéder à l'installation de etckeeper, son initialisation et de mettre en place la sauvegarde du dépôt git.
``` bash
sudo apt install etckeeper
sudo etckeeper init
sudo git init --bare /backups/etckeeper-git
sudo etckeeper vcs remote add backup /backups/etckeeper-git
sudo etckeeper vcs remote add framagit git@framagit.org:amipo/amipo-etckeeper.git
sudo etckeeper vcs branch --set-upstream-to backup/master
echo "Génération de la clé de déploiement ..."
sudo ssh-keygen -b4096
```

Dans le fichier _/etc/etckeeper/etckeeper.conf_ ajouter ou modifier la ligne :
``` conf
PUSH_REMOTE="backup framagit"
```

