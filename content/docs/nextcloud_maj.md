---
title: "Mise à jour de Nextcloud"
date: 2020-02-19T17:59:07+01:00
author: mxBossard
tags: [doc, conf, nginx, admin]
tableOfContents: true
draft: false
---

Guide de mise à jour de Nextcloud et de ses Applications.

## Désactivation des UI de mise à jour dans Nextcloud
Dans nextcloud, une mise à jour malencontreuse est vite arrivée. Il suffit de cliquer sur un bouton. Pour éviter des déploiements trop rapide d'applications ou de nouvelle versions non testées, nous avons interdit la visite des pages en question. Une erreur 403 (Forbidden) est retourné lorsque l'on tente d'y accéder.

### Nouvelle procédure de mise à jour
Voici les lignes de commandes à utiliser pour lister et réaliser les mises à jour :
- lister les commandes : `sudo -u www-data php /var/www/test/nuage/occ list`
- lister les applications : `sudo -u www-data php /var/www/test/nuage/occ app:list`
- lister les maj dispos : `sudo -u www-data php /var/www/test/nuage/occ update:check`
- update MON_APP : `sudo -u www-data php /var/www/test/nuage/occ app:update MON_APP`
- update & upgrade nextcloud : `sudo -u www-data php /var/www/test/nuage/updater/updater.phar`

### Configuration de _nginx_
Retourne une érreur 403 pour les URLs suivantes :
- /nuage/updater
- /nuage/settings/apps

Dans le fichier /etc/nginx/nextcloud.conf nous avons ajouté les lignes suivantes :
``` conf
## Interdit l'acces a la page de maj de nextcloud
location /nuage/updater {
	return 403;
}

## Interdit l'acces a la page de listing des applications
location /nuage/settings/apps {
	return 403;
}
``` 
