---
title: "Les environnements de l'Amipo"
date: 2020-02-19T18:03:15+01:00
author: mxBossard
tags: [doc, débutant]
tableOfContents: true
draft: false
---

L'AMIPO dispose actuellement de 3 environnements :
- _preview_
- _test_
- _production_
Un troisième environnement de _dev_ que chacun peu mettre en place sur son ordinateur personnel.

### L'environnement _preview_
L'environnement _preview_ est pour le moment limité à la prévisualisation du site web. Après chaque commit sur une branche de la forge git du site web, le pipeline de la forge construit le site web et le déploie dans [l'environnement _preview_](https://amipo.frama.io/amipo-site/).

### L'environnement de _test_
L'environnement de _test_ nous permet de valider le fonctionnement des services avant leur mise en _production_.
Il est typiquement utilisé pour : 
- reproduire les problèmes
- tester les mises à jour
- tester de nouvelles fonctionnalités
- tester la restauration d'une sauvegarde
Il est disponible à l'adresse https://test.amipo.fr . Il est protégé par un mot de passe pour éviter aux utilisateurs de tomber sur une version de test d'un service par hasard.

### L'environnement de _production_
L'environnement de _production_ https://amipo.fr héberge les services que nos utilisateurs utilisent. Nous évitons le plus possible d'effectuer des modifications sur cet environnement sans les avoir validées préalablement sur l'environnement de _test_.
