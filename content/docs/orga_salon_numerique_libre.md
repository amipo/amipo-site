---
title: "Organisation du salon du numrérique libre"
date: 2020-02-08T13:54:03+01:00
author: mxBossard
tags: [doc, orga]
tableOfContents: true
draft: false
---

Le 01 Fevrier 2020 à eu lieu le premier salon du numérique libre à Orléans, organisé par l'association Cenabumix.
L'AMIPO y était présent pour sensibiliser les passants, et pour communiquer autours de l'initiative des C.H.A.T.O.N.S.

## Communication
Nos objectifs en terme de communication étaient double :
- Augmenter la visibilité de l'AMIPO pour de potentiels nouveaux utilisateurs et co-administrateurs
- Diffuser la tenu prochaine de 2 Install Party co-organisé avec la Labomedia et Cenabumix

### Augmenter la visibilité de l'AMIPO
Nous avons discuté avec les passants. Leur avons parlé de l'initiative C.H.A.T.O.N.S. de Framasoft, et avons utilisé nos animations pour briser la glace.

### Diffuser les Install Party prochaines
Nous avons imprimé un flyer pour diffuser l'information aux passants. [Télécharger le flyer entreposé sur le nuage](https://amipo.fr/nuage/s/yZkqFE4xkfGkdfR)

## Animations

### Sonder les usages numériques des utilisateurs
Nous avons proposé aux visiteurs de recenser leur usages actuels de services en ligne, ou bien les services en ligne qu'ils souhaiteraient utiliser chez un acteur respectueux de leur vie privée et proche d'eux. Il en ressort les choses suivantes :

### Proposer des alternatives au GAFAM
Voici une liste de quelques C.H.A.T.O.N.S. avec la liste des services qu'ils proposent :

#### Zaclys
https://www.zaclys.com/

La plupart des services sont accessibles gratuitement et « améliorable » en devenant adhérent dès 5€ par an.
Pour voir le détail de leur offre : https://www.zaclys.com/don/

- Agenda
- Cloud 1Go gratuit ; 5Go 5€/an ; 10 Go 10€/an
- Travail collaboratif avec ONLYOFFICE
- Hebergement email 5Go pour 5€/an
- Hebergement de photos
- Envoi de gros fichiers (< 1Go)
- Aggrégateur RSS
- Messagerie instantanée
- Instance Mastodon
- « Clé USB en ligne »
- Service de pub éthique
- Messagerie dédiée à la gestion de projet et d’équipe « ZULIP » 440€ / an

#### CHAPRIL
https://www.chapril.org/

A priori pas d’adhésion, tout est librement accessible.

- Instance Mastodon
- Framadate
- Traitement de texte collaboratif (etherpad)
- Partage de copier coller (pastebin)
- Partage de fichier chiffré de bout en bout (FirefoxSend)
- Cloud

#### Colibris
https://www.colibris-outilslibres.org/

A priori pas d’adhésion, tout est librement accessible.

- Traitement de texte collaboratif (etherpad)
- Sondage (framadate)
- Wiki (YesWiki)
- Visio conférence (Jitsi-Meet)
- Messagerie instantanée (Mattermost)
- Partage de vidéos (Peertube)
- Raccourcisseur d’URL (Polr)
- Organisation via des post-it virtuels (Scrumblr / Framemo)

#### Sans nuage (ARN)
https://sans-nuage.fr/fr
Réservé aux adhérents d’ARN

- Hebergement email
- Organisation avec la méthode kanban (wekan)
- Messagerie instantanée (xmpp)
- Traitement de texte collaboratif (etherpad & libreto)
- Aggrégateur de flux RSS (ttrss)
- Cloud pour synchronisation d’équipement uniquement (pas d’hebergement de fichier) (nextcloud)
- Hébergement de diaporama (strut)
- Sondage (openSondate)


#### Framasoft (les services qui vont demeurer)
https://framasoft.org

- Traitement de texte collaboratif (etherpad)
- Sondage (framadate)
- Agenda (framagenda)
- Cloud (framadrive)
- Partage de cartes géographiques (framacarte)
- Outil d’aide à la prise de décision communautaire (Framavox)
- Partage de « cartes mentales » (framindmap)
- Visio conférence (framatalk)
