---
title: "Documentation du site web de l'Amipo"
date: 2020-02-07T15:23:37+01:00
author: mxBossard
tags: [doc, débutant]
tableOfContents: true
draft: false
---

## Principe
Le site web de l'AMIPO est construit à partir d'une arborescence de fichier texte formatés en "markdown". Tous ces fichiers textes sont consultables et modifiables dans le [repertoire content/]({{< param amipo.site.contentUrl >}}) du projet disponible sur le dépôt git. Le logiciel Hugo permet de générer un site "statique" à partir du contenu présent dans ce répertoire.

## Procédure d'ajout ou modification de contenu
Les contenus sont par défaut produit en mode brouillon __draft: true__ dans le chapeau du fichier. Les contenus en mode brouillon ne sont pas publiés sur le site. Il faut désactiver le mode brouillon en précisant __draft: false__.

### En ligne de commande (CLI) sur votre machine personelle
Verifier que le paramétrage de votre client git est correcte:
``` bash
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
Ces informations sont utilisés pour suivre qui réalise les modifications des documents du site.

#### Créez une nouvelle branche
Choisissez un nom de branche adapté à votre modification. Ci après, on symbolise le nom de cette branche avec le texte MA_NOUVELLE_BRANCHE.

Dans votre dépot du site :
``` bash
git checkout master
git pull origin master
git checkout -b MA_NOUVELLE_BRANCHE
```

#### Pour ajouter un nouveau document

##### Avec le logiciel Hugo
Avec le logiciel hugo installié sur sa machine (cf [Installation de Hugo](#installation-de-hugo)), depuis la racine du dépôt git, créez un nouveau contenu avec la commande: `hugo new docs/mon_nouveau_document.md` où __docs/mon_nouveau_document.md__ est le nom du fichier qui contiendra ce nouveau contenu. Dans notre exemple, ce contenu sera de la documentation car présent dans le repertoire docs.
- Ajoutez une nouvelle documentation : `hugo new docs/mon_nouveau_document.md`
- Ajoutez un nouvel article sur le blog : `hugo new posts/mon_nouvel_article.md`

##### Sans le logiciel Hugo
Copiez un fichier archetypale adapté présent dans le repertoire _archetypes/_ dans le bon sous repertoire content/ du dépôt git.
- Ajoutez une nouvelle documentation : `cp archetyps/docs.md content/docs/mon_nouveau_document.md`
- Ajoutez un nouvel article sur le blog : `cp archetyps/posts.md content/posts/mon_nouvel_article.md`

#### Pour modifier un document
Tous les documents sont présents dans le repertoire content/, classés hierarchiquement dans différents repertoires :
- content/docs : La documentation de l'AMIPO
- content/posts : Les articles de blog de l'AMIPO
Editez le fichier au format _markdown (.md)_

#### Commitez et pousser vos modifications
Pour commiter (proposer) vos modifications voici la liste des commandes :
``` bash
git add .
git commit -m "MON_MESSAGE_EXPLIQUANT_MON_TRAVAIL"
git push origin MA_NOUVELLE_BRANCHE
```

### Avec la forge git
La [forge git est ici]({{< param amipo.site.forgeUrl >}}). Tous les documents au format _markdown (.md)_ sont présents [dans le repertoire content/]({{< param amipo.site.forgeUrl >}}/tree/master/content)

#### Créez une nouvelle branche
Choisissez un nom de branche adapté à votre modification. Ci après, on symbolise le nom de cette branche avec le texte MA_NOUVELLE_BRANCHE.

Naviguez dans la liste des branches sur la forge : [Dépôt > Branches]({{< param amipo.site.forgeUrl >}}/-/branches) et créez une nouvelle branche à l'aide du bouton. Entrez le nom de votre branche est spécifiez la branche _master_ comme branche d'origine.

#### Pour ajouter un nouveau document
Verifiez que vous êtes bien sur votre branche, le nom de votre branche doit apparaitre dans l'URL de votre navigateur web. Copiez le contenu d'un des fichiers archetypales présents dans [le dossier archetypes/]({{< param amipo.site.forgeUrl >}}/tree/master/archetypes). Pour info :
- archetypes/docs.md : Archetype de documentation de l'AMIPO
- content/posts.md : Archetype d'un article de blog de l'AMIPO

Dans le bon sous-repertoire du repertoire content/ __de votre branche__, créez un nouveau fichier nommé __mon_nouveau_document.md__ dans lequel vous collerez le contenu de l'archetype précedement copié.

### Pour modifier un document
Tous les documents sont présents dans le repertoire content/, classés hierarchiquement dans différents repertoires :
- content/docs : La documentation de l'AMIPO
- content/posts : Les articles de blog de l'AMIPO
Ouvrez le fichier à modifier (_un fichier .md_) __dans votre branche__ grace à l'interface web de la forge git, vour pouvez cliquer sur le bouton pour ouvrir l'éditeur, soit : 
- _Edit_
- _Web IDE_

#### Commitez vos modifications
Une fois vos modifications dans le fichier terminé, utilisez l'interface web pour entrer un message décrivant vos modifications (le _Commit message_) et cliquez sur le bouton de validation de vos modifications : __Commit changes__.

### Proposez la fusion de vos modifications
Lorsque toutes les modifications que vous souhaitiez effectuer sont terminées et commitées, vous pouvez créer une _Merge request_ depuis l'interface web de la forge. Pour cela, cliquez sur le menu "__Merge Requests__" à gauche de l'interface. 
1. _Dans ce menu, si votre branche est toujours sélectionnée_, vous pouvez cliquer sur le bouton "__Create merge request__". 
1. _Si votre branche n'est pas sélectionnée_, cliquez sur le bouton "__New merge request__" et utilisez l'interface pour sélectionner votre branche comme source et la branche master comme cible et enfin cliquez sur le bouton "__Compare branches and continue__".

Enfin, choisissez soigneusement un titre et une description résumant toutes les modifications présentes sur votre branche, puis cliquez sur le bouton "__Submit merge request__".

Vos modifications sont désormais proposez, un mainteneur du projet pourra entamer des discussions avec vous pour vous guider, améliorer ou rejeter votre proposition.

## Visualisation

### Avec le logiciel Hugo
Avec le logiciel hugo installié sur sa machine (cf [Installation de Hugo](#installation-de-hugo)), depuis la racine du dépôt git, on peut lancer la génération du site web avec la commande: `hugo server`.

### En "preview" sur les pages gitlab
Après un commit sur votre branche, le site web est généré par le pipeline de la forge git. Le travail du pipeline est indiqué par un badge "pipeline" clicable situé sur la page d'[accueil du projet]({{< param amipo.site.forgeUrl >}}). Une fois le pipeline déroulé, la prévisualisation du site web est disponible [ici]({{< param amipo.site.previewUrl >}}).

### En "test" après la fusion de votre branche sur la branche master
Une fois vos propostion accépté et votre branche fusioné sur la branche master, le pipeline construi le site web de "production" et le déploie sur l'environnement de test https://test.amipo.fr/

## Installation de Hugo
Pour installer Hugo on peut suivre [ce guide](https://gohugo.io/getting-started/installing/).

## Pipeline d'intégration & de déploiement
Les pipeline de d'intégration et de déploiement sont configurés dans le fichier .gitlab-ci.yml à la racine du dépôt git.

### Proposition
Lorsque la forge git reçoit un commit sur une branche (quelque soit la branche hormis master et preview), un pipeline de test lancé et permet de valider la syntaxe des fichiers sur le projet. Si le pipeline échoue, cela veut dire que le logiciel Hugo à rencontré des problèmes pour construire de le site web avec les sources que vous avez modifiés. Vous devriez recevoir un email pour vous signifier ce fait. Une fois le pipeline terminé, le site web construit sera déployé sur un environnement "preview" [accessible ici]({{< param amipo.site.previewUrl >}}).

### Master
Lorsqu'une proposition est fusionnée sur cette branche, le pipeline tente de construire le site web à déstination de la production. Si le pipeline termine avec succès, alors le site web static généré est disponible au [format zip en téléchargement]({{< param amipo.site.prodArtifactUrl >}}). Le site web de production sera alors automatiquement déployé sur l'environnement de test https://test.amipo.fr/ dans la minute qui suit.

Pour le dépolyer en production, il faut se logguer sur le serveur puis 
``` bash
sudo su
cd /var/amipo-admin-code/
./promote_en_prod.sh /var/www/test/html
```


