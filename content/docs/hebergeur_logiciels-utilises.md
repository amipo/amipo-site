---
title: "Hébergeur de l'AMIPO et logiciels utilisés"
date: 2020-03-05T18:01:54+01:00
tags: [doc, conf]
tableOfContents: true
draft: false
---

## Hébergeur de la machine virtuelle utilisée par l'AMIPO

L'AMIPO loue une machine virtuelle chez les sympathiques personnes d'Alsace Réseau Network (Merci!) https://vps.arn-fai.net/

### Comment ça marche chez ARN ? (Technologies utilisées)
KVM pour l'hyperviseur ; Ganeti pour la redondance : en cas de panne matérielle sur l'un de nos serveurs, votre VPS sera transféré sur un autre serveur. 
De plus, aucune interruption de service à prévoir lors d'une maintenance de notre côté.

### Où se trouve le datacenter qui héberge les serveurs d'ARN ?

Il s'agit du datacenter Cogent situé 46 route de Bischwiller à Schiltigheim, Alsace, France.

### Quel est le débit ?

Capacité max. : 100 Mbps mutualisés entre tous les abonné-e-s, aucune limite de volume.

### Est-ce qu'ARN conserve des informations / traces / logs ?

Oui, tel que requis par la législation française : votre identité = (Administrateurs AMIPO : prénom + nom + adresse postale + numéro de téléphone) + les adresses IP assignées par ARN à votre VPS. Pas plus, pas moins. Tout cela est conservé pendant 1 an. ARN se réserve le droit de vérifier la véracité de votre identité.


## Machine virtuelle de l'AMIPO chez ARN
### VPS
Elle s'agit d'une VPS "STO-2G-200"
- 2vcpu
- 2Go de RAM
- 200Go HDD (ou moins)
- 1 adresse IPv4
- 1 adresse IPv6

### OS et logiciels utilisés
Sur cette machine, nous avons installé une Debian Stable 10 buster en tant que système d'exploitation de l'ordinatrice

Nous utilisons des logiciels idéalement libres ou, à défaut, dont les sources sont ouvertes en essayant autant que possible de faire correspondre les choix techniques aux valeurs et idées auquelles nous adhérons notamement à travers le [Manifeste Chatons](https://chatons.org/manifeste)

Soit (liste non exhaustive) :
- NGINX, PHP, Postgres
- Python, Ibniz

Nous apportons une attention toute particulière à la question de la sécurité et à celle de l'intégrité des données, du respect de la vie privée. Nous sommes loin d'être infaillibles et les logiciels aussi.
L'AMIPO, c'est une forme d'informatique paysanne qui prend le temps de comprendre les outils et essaye de les améliorer, il faut prévoir des plantages.


