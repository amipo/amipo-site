---
title: "Premier salon du numrérique libre Orleanais"
date: 2020-02-01
author: mxBossard
tags: [event, cr]
draft: false
---

Le Samedi 1° Fevrier 2020 avait lieu le premier salon du numérique libre Orléanais, organisé par l'association Cenabumix.

L'AMIPO y était présente pour :
- nouer le contact avec les acteurs du libre de la région
- dialoguer avec des usagers du numérique et leur faire découvrir l'AMIPO et plus largement l'initiative des C.H.A.T.O.N.S.
- diffuser la tenue prochaine de 2 Install Party co-organisé par l'AMIPO, la Labomedia et Cenabumix

[La documentation de notre organisation est disponible ici](/docs/orga_salon_numerique_libre).
