---
title: "En novembre 2020 ..."
date: 2020-11-02T16:01:04+01:00
author: b01
tags: [news, event]
draft: false
---

# Des nouvelles de l'AMIPO
Reconfinement ... derrière des écrans ... occasion d'adopter des solutions libres, d'adopter des [chatons](https://chatons.org/fr/find-by-services)

Du coté de l'AMIPO, ça ronronne, après une panne malencontreuse (un changement de nom d'interface réseau ?!) qui a interrompu nos valeureux services pendant une durée non déterminée, veuillez nous en excuser et rejoignez-nous pour que cela n'arrive plus en vous inscrivant à notre mailing-list https://listes.amipo.fr/sympa/subscribe/miaou

Peut-être allons-nous profiter de ces temps particuliers pour [manger des tickets](https://framagit.org/groups/amipo/-/issues) et nous abreuver de ressources libres et éthiques.


