---
title: "Install party du Jeudi 6 Fevrier 2020"
date: 2020-02-06
author: mxBossard
tags: [event, cr]
draft: false
---

Le Jeudi 6 Fevrier, la Labomedia accueillait une nouvelle Install Party. Pour cette seconde Install Party, il a été fait le choix de nous retrouver un Jeudi après midi durant les Open Atelier de la Labomedia, tout près du fameux "Atelier du C01N", le hackerspace "proto fablab" de la Labomedia.

L'evenement s'est déroulé de manière satisfaisante. Nous y avons croisé 6 personnes venu pour découvrir le libre, discuter, partager un bon moment et libérer leur usages numériques. Nous avons notament réussi à installer un Linux Mint Mate sur un Macbook Pro (hormis le son un poil récalcitrant). [La documentation de cette installation particulière est disponible ici](https://ressources.labomedia.org/linux_on_macbookpro).

Les visiteurs on partagés le traditionnel "bit de dieu" avec les habitués du C01N.

Merci encore à [Cenabumix](https://wiki.cenabumix.org/wordpress/) et à [la Labomedia](https://labomedia.org) pour la co-organisation de cet évenement.
