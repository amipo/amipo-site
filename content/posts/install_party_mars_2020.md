---
title: "Install party du Samedi 7 mars 2020 dès 09h"
date: 2020-02-08T16:01:04+01:00
author: mxBossard
tags: [news, event]
draft: false
---

__Le Samedi 7 Mars, dès 09h du matin__ aura lieu le dernièr épisode de notre trilogie d'Install Party co-organisé par l'AMIPO, la Labomedia et Cenabumix. Cet évenement aura encore une fois lieu __au 108 rue de bourgogne__, soit dans la salle 109 au rez-de-chaussez, soit dans la salle D1 au 3° étage.

{{< figure src="/posts/install_party_2020/install_party_AMIPO_Cenabumix_flyer.png" alt="Flyer Install Party 2020" caption="Flyer des Install Party" >}}

# Comment participer ?
Pour faciliter votre prise en charge, nous vous demandons si cela vous est possible de :
1. Vous inscrire
2. Sauvegarder vos données
3. Défragmente votre disque dur

## Inscrivez-vous 
Inscrivez-vous par email à l'adresse infos [at] amipo.fr Précisez :
1. Ce que vous souhaitez réaliser avec nous ?
2. Quel sont le modèle et l'age de votre matériel si vous désiré en apporter.

## Sauvegardez vos données
Si vous disposez d'un suport de stockage de données externes (clé USB, disque dur externe, ...), copiez-y les données importantes que vous souhaité conserver avant de venir nous voir. C'est une opéartion qui peut être longue.

## Défragmenter votre disque dur
Si vous souhaitez installer Linux que que vous windows est istallé sur votre machine, essayez de lancer la défragmentation de votre disque dur. Cette opération pourra accélerer l'installation de linux sur votre machine.

# Vous souhaiter tester linux chez vous ?
Apporter nous une clé USB d'au moins 4 Go que nous pourrons effacer pour y déposer une version de linux testable chez vous sans installation.

