---
title: "CAC du 30 janvier 2020"
date: 2020-01-31T16:50:59+01:00
author: mxBossard
tags: [cr]
---

# CAC du 30 janvier 2020

## Personnes présentes

Max, Jérémy

## Compte rendu

* Présentation du pipeline de build du site web
* Maj du site web : ajout du menu
* Publication d'une premiere doc et d'un CR
* Verification check list salon du numérique libre
* Vote favorablement pour la charte des chatons v2
* Création de dé events pour les install party sur l'agenda du libre
