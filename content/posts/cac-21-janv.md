---
title: "CAC du 21 janvier 2020"
date: 2020-01-28T14:10:59+01:00
author: bCadon
tags: [cr]
---

# CAC du 21 janvier 2020

Max, Benjamin, Jérémy, Emmanuel

# Framagit

Présentation de l'utilisation de Gitlab : [Framagit](https://framagit.org/amipo/)

## Amipo-plante 

Utilisation des "issues" ou tickets pour proposer une idée, soumettre un problème, poser une question, prévoir des choses à faire Quand on crée une nouvelle issue, on peut ajouter dans le message @all pour que tt les gens inscrits reçoivent une notification par mail (ou @mxbossard pour pinguer Max)

Possibilité d'utiliser Amipo plante pour :

Quand AMIPO plante, contactez nous ici. Donnez-nous vos retours utilisateurs. Vous pouvez déclarer un problème via l'interface web : en cliquant ici. La liste des sujets sur lesquels nous discutions et travaillons est ici. Ou encore déclarer un problème en nous envoyant un email à cette adresse incongrue ci-après : [gitlab-incoming+amipo-amipo-plante-55401-JtTxU6rv7nCVf_mEnbzv-issue@framagit.org](mailto:gitlab-incoming+amipo-amipo-plante-55401-JtTxU6rv7nCVf_mEnbzv-issue@framagit.org)

* les [labels](https://framagit.org/amipo/amipo-plante/-/labels)
* les [tableaux](https://framagit.org/amipo/amipo-plante/-/boards) qui reprennent les labels
* les [milestones ou jalons](https://framagit.org/amipo/amipo-plante/-/milestones)

# Charte v2 

Jusqu'au 1er février pour voter sur la charte v2 https://framagit.org/amipo/amipo-plante/issues/26

Ouverture de tickets

# Journée du numérique libre 

* Notre présence: 14h -> 17h **Samedi 1er février** [journée numérique libre](https://wiki.cenabumix.org/wordpress/2020/01/10/salon-du-numerique-libre/)
* Salle en bas de la rue du pressoir neuf
* Quadrature + April invités
* Communiquer sur les install party à venir
* Faire une doc pour l'utilisation de Nextcloud pour utilisateur final
* Faire un sondage sur les services que les gens souhaiteraient retrouver
* Lister les services Frama qui vont fermer https://framablog.org/2019/09/24/deframasoftisons-internet
* le calendrier de fermeture https://framablog.org/wp-content/uploads/2019/09/Planning-fr-v2.png https://framaclic.org/h/fermeture-services
* Lister les services framasoft qui resteront ouvert ainsi que les services de quelques chatons alentours pour pouvoir les recommander aux gens.

Autres chatons :

* [Zaclys](https://www.zaclys.com/)
* [Chapril](https://www.chapril.org/)
* [Colibri](https://www.colibris-outilslibres.org/)
* [Roflcopter](https://wtf.roflcopter.fr/apps/)
* [Arn](https://sans-nuage.fr/fr)

# Site web

* Question de la sécurité du CMS utilisé actuellement
* Question de sa compatibilité avec les standards sémantiques, d'accessibilité

Générateur de site statique :

- [Hugo](https://gohugo.io/)
- [Pelican](https://blog.getpelican.com/)

Le site avec le CMS de joseph : https://amipo.fr/nouveau-site/

Pour se "logguer" https://amipo.fr/nouveau-site/admin/ + pwd

### Page accueil

* AMIPO ?
* Quels outils ?
* Dans quel but ?
* Qui sommes nous ?
* Plus d'informations ?
* Participer ?
* Calendrier des activités
* Nous contacter ? 

### Engagements

Réponses aux point de la charte v2 (documentation, ..)

### CGU

TODO

### Mentions légales

TODO

### L'association

* status
* membres
* compte rendus

## Pour contenus

Charte des chatons

* le CHATON s’engage à indiquer publiquement quelle est son offre de service, ainsi que les tarifs associés ;
* le CHATON s’engage à publier des Conditions Générales d’Utilisations (CGU) visibles, claires, non ambigües, et compréhensibles par le plus grand nombre ;
* le CHATON s’engage à publier, dans ses CGU, une clause « Données personnelles et respect de la vie privée » indiquant clairement quelle est la politique du CHATON concernant les pratiques visées ;
* le CHATON s’engage à rendre publics ses rapports d’activités, au moins pour la partie concernant son activité de CHATON ;
* le CHATON s’engage à porter à la connaissance des hébergées les principales informations concernant sa politique de sécurité et de sauvegarde (en veillant évidemment à ce que ces informations ne portent pas atteinte à ladite politique de sécurité) ;
* le CHATON s’engage à communiquer avec les hébergées sur les difficultés qu’il rencontre dans l’exploitation de sa structure et des différents services qu’il met à disposition, notamment à travers la mise en place de services de support et d'incidents.

## Plan d'attaque pour la Journée du Numérique Libre

1. Produire le contenu du site web
2. Lister les services de chatons alentours
3. Produire une liste de services dont on veut savoir si ils interessent les gens
4. Apporter un cahier pour laisser libre à l'imagination des gens
5. Essayer de passer le calendar en version 2.0.1
6. Communiquer sur les prochaines Install Party

Et merci :)

## Mise à jour de NextCloud 18


	sudo su -l www-data -s /bin/bash

https://help.nextcloud.com/t/problems-getting-community-document-server-to-run/68338/2
