---
title: "Amipo ?"
description: "Avec l'AMIPO, décentralisons et relocalisons nos services Internet dans la région Orléanaise. Apprenons à héberger l'Internet autrement et à renforcer notre vie privée."
date: 2020-01-26
---

L'Association de Maintien de l'Informatique Paysanne Orléanaise est une initiative collective d’usagers de l’Internet pour la relocalisation de nos outils Internet, la revendication de notre droit à la vie privée et l’éducation numérique populaire.

L’AMIPO adhère à la [charte](https://chatons.org/fr/charte) C.H.A.T.O.N.S et promeut son [manifeste](https://chatons.org/fr/manifeste).

# Clôture des activités de l'AMIPO en juin 2022



Après une jolie aventure collective à travers laquelle nous avons appris plein de choses et recontré / sensibilisé un bon nombre de personnes, notre petit C.H.A.T.O.N.S. s'est quelque peu essouflé, il est peu être temps de le laisser s'endormir pour de nouvelles aventures.

Merci à celles et ceux qui se sont impliquées et intéressées à ce projet

Pour prolonger cette initiative, La Labomedia, association orléanaise impliquée dans l'AMIPO, a lancé le projet https://futuretic.fr à destination de collectifs et d'associations avec un certain nombre d'outils en ligne accessibles librement

Miaou

# Archivage du site pour mémoire

## Quels outils ?

Nous vous proposons un petit nuage pour y déposer vos contacts, agendas et listes de tâches.

**Pour utiliser ce petit nuage, créez un compte ici :  https://amipo.fr/nuage/**

**Si vous êtes déjà inscrit·e·s, vous pouvez vous connecter ici : https://amipo.fr/nuage/login**

L'utilisation de ce nuage est totalement gratuite (et idéalement sécurisée), vous pouvez néanmoins adhérer à l'association pour soutenir le projet et pourquoi pas aussi y contribuer.


## Dans quel but ?

Nous souhaitons proposer des services numériques locaux, respectueux de la vie privée de ses utilisateurs. Nous croyons en l’éducation populaire et pensons qu’il est profitable à chacun de mieux comprendre ces outils pour mieux les utiliser. Notre mouvement s’inscrit dans la mouvance initiée par [Framasoft](https://framasoft.org/fr/) et sa campagne « Dégooglisons internet ». Nous voulons contribuer de façon artisanale à la décentralisation de l’Internet, tout en favorisant le partage de connaissances et le respect de l'intimité numérique.



## Qui sommes-nous ?

Nous sommes des personnes qui se retrouvent dans les valeurs promues par le manifeste C.H.A.T.O.N.S. réunies sous la forme d'une association à but non lucratif (loi 1901). À ce titre, nous désirons un Internet transparent, neutre, ouvert et solidaire. Nous souhaitons comprendre, transmettre, apprendre les rouages à l’œuvre dans les nuages que nous utilisons tous les jours. Notre association est d’abord un collectif, présidé par un Conseil d’administration collégial ouvert à tous. Nous cultivons le consensus pour faire mûrir nos décisions.


## Plus d'informations ?

Nous nous attachons à être transparents et à documenter les outils mis en place, retrouvez ces informations à cette adresse : [https://amipo.fr/docs/](https://amipo.fr/docs/).


## Participer ?

Il y a beaucoup de manières de participer :

* en s'impliquant dans la mise en place et la gestion des outils, soit en tant que néophyte pour mieux comprendre comment fonctionne les services en ligne et plus généralement Internet, ou en tant qu'expert pour contribuer à une noble cause,
* en participant à la création et à la diffusion de ressources pédagogiques auprès du grand public,
* en contribuant à des actions de communication et de sensibilisation.


Si vous êtes intrigué·e·s, intéréssé·e·s ou encore si vous désirez mieux comprendre les enjeux du numérique tout azimut, n’hésitez pas à nous contacter.

## Nous contacter ?


Vous pouvez nous écrire à l’adresse : `infos [chez]* amipo.fr`

Et si vous souhaitez être tenu·e informé·e de nos activités par courriel, vous pouvez vous abonner à notre liste de diffusion en envoyant un courriel vide à cette adresse : `miaou-subscribe [chez]* listes.amipo.fr`

 ou bien en utilisant l'interface graphique située à cette adresse : https://listes.amipo.fr/sympa/subscribe/miaou

_\* N’oubliez pas de remplacer [chez] par @._

L'association AMIPO est basée au 108 rue de Bourgogne à Orléans / France


Une initiative soutenue par : [Le 108](https://le108.org/), [Framasoft](https://framasoft.org/fr/), [La Labomedia](https://labomedia.org/).
